=============================
PETSc |version| Documentation
=============================

.. Important::

  The PETSc docs are currently in transition,
  and different pieces "live" in two different places. In addition to on this page,
  much of the documentation is found `here <https://www.mcs.anl.gov/petsc/documentation/index.html>`__.

  Highly recommended resource: `PETSc for Partial Differential Equations: Numerical Solutions in C and Python, By Ed Bueler <https://my.siam.org/Store/Product/viewproduct/?ProductId=32850137>`__,

.. toctree::
   :maxdepth: 1

   manual/index
   guides/guide_to_examples
   developers/index
